#!/usr/bin/env python
import rospy
import gripper3

from std_msgs.msg import String
from std_msgs.msg import Bool
from std_msgs.msg import Int32

samples = 10
threshold = 10
positionThreshold = 8

pressureData = [0 for _ in xrange(samples)]
positionData = [0 for _ in xrange(samples)]
initialCounter = 0

publisher = rospy.Publisher('/gripper/slip', Bool, queue_size=1)
posSlip = rospy.Publisher('/gripper/pos_slip', Bool, queue_size=1)
presSlip = rospy.Publisher('/gripper/pres_slip', Bool, queue_size=1)
predictionPub = rospy.Publisher('/gripper/prediction', Int32, queue_size=1)

def main():
    rospy.init_node("grasp_slip_simple")
    rospy.Subscriber('/gripper/serial/output', String, SerialHandler)
    rospy.spin()


def SerialHandler(msg):
    if msg.data.startswith("tact:"):
        global initialCounter

        input_data = msg.data.split(":")[1].split(",")
        pressure = int(input_data[0].strip())
        position = int(input_data[1].strip())

        if initialCounter < samples:
            initialCounter = initialCounter + 1
        else:
            avgPress = sum(pressureData)/samples
            avgPos = sum(positionData)/samples
            print("average pressure:{}".format(avgPress))
            print("average position:{}".format(avgPos))
            diffPress = abs(pressure-avgPress)
            diffPos = abs(position - avgPos)
            print("difference with the new pressure".format(diffPress))
            print("difference with the new position".format(diffPos))
            slipPress = bool(diffPress > threshold)
            presSlip.publish(slipPress)
            slipPos = bool(diffPos > positionThreshold)
            posSlip.publish(slipPos)
            slip = slipPress or slipPos
            print(slip)
            publisher.publish(slip)
            if(slip):
                predictionPub.publish(Int32(500))
            else:
                predictionPub.publish(Int32(0))
        pressureData.pop()
        pressureData.insert(0, pressure)
        positionData.pop()
        positionData.insert(0, position)


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
