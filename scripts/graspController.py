import rospy
import gripper3
from std_msgs.msg import Bool
import time


closingState = False
gripper = None
force = 30
lastIncrease = time.time()


def main():
    rospy.init_node("grasp_controller")
    rospy.Subscriber('/gripper/slip', Bool, SlipHandler)
    global gripper, closingState
    gripper = gripper3.Gripper3("/gripper/serial/input")
    rospy.sleep(1)
    gripper.close(force)
    closingState = True
    # grasp
    rospy.spin()


def SlipHandler(slip):
    global gripper, force, lastIncrease
    if closingState and slip.data and time.time() - lastIncrease > 0.1:
        force += 10
        gripper.close(force)
        print(force)
        lastIncrease = time.time()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
