import rospy
from std_msgs.msg import String

class Gripper3:
    _publisher = 0

    def __init__(self, gripperCommandsTopic) :
        if not rospy.get_node_uri():
            raise AssertionError("node must be started")
        self._publisher = rospy.Publisher(gripperCommandsTopic, String, queue_size=1)

    def open(self):
        self._publisher.publish(String("1c100s"))

    # from 
    def close(self, power):
        self._publisher.publish(String("3c{}".format(power)))